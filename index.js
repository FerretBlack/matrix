function setRandomNumber() {
    let n = Math.floor(Math.random() * Math.random() * 100) + 1 
    setMatrix(n)
}

function setMatrix(number) {
    let size = 2 * number - 1
    let matrix = []

    for(let i = 0; i < size; i++) {
      matrix[i] = []

      for(let j = 0; j < size; j++) {
        matrix[i][j] = Math.floor(Math.random() * Math.random() * 100) + 1
      }
    }
    //создали матрицу и отправляем ее в функцию для дальнейшей обработки 
    setSpiral(matrix)
}

function setSpiral(matrix) {
    let newArr = []

    while (matrix.length) {
      newArr.push(...matrix.shift()) // здесь возвращаем и удаляем первый элемент массива
      matrix.map(row => newArr.push(row.pop())) //возвращаем и удаляем последние элементы вложенных массивов
      matrix.reverse().map(row => row.reverse()) // переворачиваем и возвращаем 
    }

    console.log(newArr)
    // развернул матрицу просто по часовой стрелке от левого верхнего угла по спирали к центру. 
 
  }

setRandomNumber()
